﻿using System;
using System.Threading;
using System.Diagnostics.Contracts;

namespace PenguinNetworking.Utility
{
    static class LockExtensionMethods
    {
        public interface IDisposableLock : IDisposable
        {
            bool IsReader { get; }
            bool IsWriter { get; }

            void Release();
            void UpgradeToWriter();
        }


        sealed class LockToken : IDisposable
        {
            private object _sync;
            public LockToken(object sync)
            {
                _sync = sync;
                Monitor.Enter(_sync);
            }
            public void Dispose()
            {
                if (_sync == null) return;
                Monitor.Exit(_sync);
                _sync = null;
            }
        }

        private sealed class ReadLockToken : IDisposable
        {
            private ReaderWriterLock _sync;
            public ReadLockToken(ReaderWriterLock sync)
            {
                Contract.Requires(sync != null);
                _sync = sync;
                sync.AcquireReaderLock(Timeout.Infinite);
            }
            public void Dispose()
            {
                if (_sync == null) return;
                _sync.ReleaseReaderLock();
                _sync = null;
            }
        }

        public sealed class WriteLockToken : IDisposable
        {
            private ReaderWriterLock _sync;
            public WriteLockToken(ReaderWriterLock sync)
            {
                Contract.Requires(sync != null);
                _sync = sync;
                sync.AcquireWriterLock(Timeout.Infinite);
            }
            public void Dispose()
            {
                if (_sync == null) return;
                _sync.ReleaseWriterLock();
                _sync = null;
            }
        }

        public sealed class ReadLockSlimToken : IDisposable
        {
            private ReaderWriterLockSlim _sync;
            public ReadLockSlimToken(ReaderWriterLockSlim sync)
            {
                Contract.Requires(sync != null);
                _sync = sync;
                sync.EnterReadLock();
            }
            public void Dispose()
            {
                if (_sync == null) return;
                _sync.ExitReadLock();
                _sync = null;
            }
        }

        public sealed class WriteLockSlimToken : IDisposable
        {
            private ReaderWriterLockSlim _sync;
            public WriteLockSlimToken(ReaderWriterLockSlim sync)
            {
                Contract.Requires(sync != null);
                _sync = sync;
                sync.EnterWriteLock();
            }
            public void Dispose()
            {
                if (_sync == null) return;
                _sync.ExitWriteLock();
                _sync = null;
            }
        }

        /// <summary>
        /// Obtain a read lock on the <seealso cref="ReaderWriterLockSlim"/>.
        /// </summary>
        /// <param name="obj"><seealso cref="object"/> obj.</param>
        /// <returns></returns>
        public static IDisposable ReadLock(this object obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var lockSlim = obj as ReaderWriterLockSlim;
            if (lockSlim != null)
                return new ReadLockSlimToken(lockSlim);

            var rwLock = obj as ReaderWriterLock;
            if (rwLock != null)
                return new ReadLockToken(rwLock);

            return new LockToken(obj);
        }

        /// <summary>
        /// Obtain a write lock on the <seealso cref="ReaderWriterLockSlim"/>.
        /// </summary>
        /// <param name="obj"><seealso cref="object"/> obj.</param>
        /// <returns></returns>
        public static IDisposable WriteLock(this object obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var lockSlim = obj as ReaderWriterLockSlim;
            if (lockSlim != null)
                return new WriteLockSlimToken(lockSlim);

            var rwLock = obj as ReaderWriterLock;
            if (rwLock != null)
                return new WriteLockToken(rwLock);

            return new LockToken(obj);
        }
    }

}
