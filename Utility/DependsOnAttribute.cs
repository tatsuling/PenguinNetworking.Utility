﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinNetworking.Utility
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class DependsOnAttribute: Attribute
    {
        public DependsOnAttribute(string property)
        {
            Property = property;
        }

        public string Property { get; private set; }
    }
}
