﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinNetworking.Utility
{
    public interface IDeepCopy
    {
        /// <summary>
        /// Create a deep copy of the object.
        /// </summary>
        /// <returns></returns>
        object DeepCopy();
    }
}
