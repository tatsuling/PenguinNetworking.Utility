﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinNetworking.Utility
{
    public static class SupportInitializeExtension
    {
        public static IDisposable Init(this ISupportInitialize isi)
        {
            if (isi == null) return null;
            return new SupportInitializeDisposable(isi);
        }

        private class SupportInitializeDisposable : IDisposable
        {
            private ISupportInitialize InitializeObject { get; }

            internal SupportInitializeDisposable(ISupportInitialize o)
            {
                InitializeObject = o;
                InitializeObject.BeginInit();
            }

            void IDisposable.Dispose()
            {
                InitializeObject.EndInit();
            }
        }

        public static Task<bool> WaitForInitialized(this ISupportInitializeNotification isin)
        {
            if (isin == null)
            {
                return Task.FromResult(false);
            }

            var tcs = new TaskCompletionSource<bool>();
            isin.Initialized +=
                (sender, e) =>
                {
                    tcs.SetResult(true);
                };

            if (isin.IsInitialized)
            {
                tcs.SetResult(true);
            }

            return tcs.Task;
        }

    }
}
