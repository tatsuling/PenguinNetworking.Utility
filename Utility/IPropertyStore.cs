﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace PenguinNetworking.Utility
{
    [ContractClass(typeof(PropertyStoreContract))]
    public interface IPropertyStore
    {
        IDictionary<string, object> PropertyValuesDictionary { get; }
        object SyncRoot { get; }
    }

    [ContractClassFor(typeof(IPropertyStore))]
    public abstract class PropertyStoreContract : IPropertyStore
    {
        private readonly Dictionary<string, object> _propertyValuesDictionary = new Dictionary<string, object>();

        public IDictionary<string, object> PropertyValuesDictionary
        {
            get
            {
                Contract.Ensures(Contract.Result<IDictionary<string, object>>() != null);
                return _propertyValuesDictionary;
            }
        }

        public object SyncRoot
        {
            get
            {
                Contract.Ensures(Contract.Result<object>() != null);
                return ((System.Collections.ICollection)_propertyValuesDictionary).SyncRoot;
            }
        }
    }

    public static class PropertyStoreExtension
    {
        /// <summary>
        /// Get a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to return the object as.</typeparam>
        /// <param name="propertyName">Name of the property to get.</param>
        /// <param name="propertyStore">Object that will store the properties.</param>
        /// <param name="Default">A default value to return if the property value has not been set yet.</param>
        [DebuggerStepThrough]
        public static T GetPropertyStoreValue<T>(this IPropertyStore propertyStore, T Default = default(T), string propertyName = "")
        {
            using (propertyStore.SyncRoot.ReadLock())
            {
                object ret;
                if (propertyStore.PropertyValuesDictionary.TryGetValue(propertyName, out ret))
                {
                    return (T)ret;
                }
            }

            return Default;
        }

        /// <summary>
        /// Get a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to return the object as.</typeparam>
        /// <param name="propertyName">Name of the property to get.</param>
        /// <param name="propertyStore">Object that will store the properties.</param>
        /// <param name="Default">A function to call to get a default value to return if the property value has not been set yet.</param>
        [DebuggerStepThrough]
        public static T GetPropertyStoreValue<T>(this IPropertyStore propertyStore, Func<T> Default, string propertyName = "")
        {
            using (propertyStore.SyncRoot.ReadLock())
            {
                object ret;
                if (propertyStore.PropertyValuesDictionary.TryGetValue(propertyName, out ret))
                {
                    return (T)ret;
                }
            }

            {
                var ret = Default();
                using (propertyStore.SyncRoot.WriteLock())
                {
                    propertyStore.PropertyValuesDictionary[propertyName] = ret;
                }

                return ret;
            }
        }

        /// <summary>
        /// Set a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to return the object as.</typeparam>
        /// <param name="propertyStore">Object that will store the properties.</param>
        /// <param name="value">The new value of the property.</param>
        /// <param name="propertyName">Name of the property to set.</param>
        /// <param name="throwOnSetAfterInit">Check if the initialization is done and throw if this is not allowed.</param>
        /// <returns>Retuns true if the new value is different than the old value. (i.e. the value changed)</returns>
        [DebuggerStepThrough]
        public static bool SetPropertyStoreValue<T>(this IPropertyStore propertyStore, T value, string propertyName = "", bool throwOnSetAfterInit = false)
        {
            object old;
            bool found;

            if (throwOnSetAfterInit)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                var isin = propertyStore as ISupportInitializeNotification;
                if (isin != null && isin.IsInitialized)
                {
                    throw new InvalidOperationException($"Unable to set property {propertyName} after initialization is complete.");
                }
            }

            using (propertyStore.SyncRoot.WriteLock())
            {
                found = propertyStore.PropertyValuesDictionary.TryGetValue(propertyName, out old);
                propertyStore.PropertyValuesDictionary[propertyName] = value;
            }

            return !(found && (old is T) && EqualityComparer<T>.Default.Equals((T)old, value));
        }
    }
}
