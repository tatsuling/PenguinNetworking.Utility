﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace PenguinNetworking.Utility
{
    /// <summary>
    /// Base class for all Model classes in the application.
    /// It provides support for property change notifications.
    /// This class is abstract.
    /// </summary>
    /// 
    public abstract class ObservableObject :
        INotifyPropertyChanged,
        IPropertyStore,
        IPropertyVerification,
        ISupportInitializeNotification
    {
        protected ObservableObject()
        {
            IsInitializing = false;
            IsInitialized = true;

            SetupDependsOnMapping();
            PropertyChanged += (s, e) =>
            {
                if (!dependsOnMapping.ContainsKey(e.PropertyName)) return;
                foreach (var property in dependsOnMapping[e.PropertyName])
                {
                    Debug.Print($"{this.GetType().Name} - {e.PropertyName} => {property}");
                    OnPropertyChanged(property);
                }
            };
        }

        private void SetupDependsOnMapping()
        {
            var dependsOnMapping = new Dictionary<string, IList<string>>();

            var allProperties = GetType().GetProperties(BindingFlags.FlattenHierarchy |
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var pi in allProperties)
            {
                var dpas = pi.GetCustomAttributes(typeof(DependsOnAttribute)).Cast<DependsOnAttribute>();
                foreach (var dpa in dpas)
                {
                    if (!dependsOnMapping.ContainsKey(dpa.Property))
                    {
                        dependsOnMapping.Add(dpa.Property, new List<string>());
                    }
                    dependsOnMapping[dpa.Property].Add(pi.Name);
                    Debug.Print($"{this.GetType().Name} - {dpa.Property} => {pi.Name}");
                }
            }

            this.dependsOnMapping = dependsOnMapping;
        }

        private IDictionary<string, IList<string>> dependsOnMapping;

        #region Property helpers

        public IDictionary<string, object> PropertyValuesDictionary { get; } = new Dictionary<string, object>();

        private readonly Lazy<ReaderWriterLockSlim> _lockObj = new Lazy<ReaderWriterLockSlim>();
        public object SyncRoot => _lockObj.Value;

        /// <summary>
        /// Get a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to return the object as.</typeparam>
        /// <param name="propertyName">Name of the property to get.</param>
        /// <param name="Default">A default value to return if the property value has not been set yet.</param>
        protected T GetValue<T>(T Default = default(T), [CallerMemberName] string propertyName = "")
        {
            return this.GetPropertyStoreValue(Default, propertyName);
        }

        /// <summary>
        /// Get a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to return the object as.</typeparam>
        /// <param name="propertyName">Name of the property to get.</param>
        /// <param name="Default">A function to call to get a default value to return if the property value has not been set yet.</param>
        protected T GetValue<T>(Func<T> Default, [CallerMemberName] string propertyName = "")
        {
            return this.GetPropertyStoreValue(Default, propertyName);
        }



        /// <summary>
        /// Set a property value that is stored in the internal dictionary of the ViewModel object.
        /// </summary>
        /// <typeparam name="T">Type to store the object as.</typeparam>
        /// <param name="value">The new value of the property.</param>
        /// <param name="propertyName">Name of the property to set.</param>
        /// <param name="throwOnSetAfterInit">Throw if the property is set after initialization is complete.</param>
        /// <returns>Returns true if the property was changed.</returns>
        protected bool SetValue<T>(T value, [CallerMemberName] string propertyName = "", bool throwOnSetAfterInit = false)
        {
            var updated = this.SetPropertyStoreValue(value, propertyName);
            if (updated)
            {
                OnPropertyChanged(propertyName);
            }
            return updated;
        }
        #endregion // Property helpers

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            if (IsInitializing)
            {
                lock (LostOnPropertyNotification)
                {
                    LostOnPropertyNotification.Add(propertyName);
                }
            }
            else
            {
                //var e = new PropertyChangedEventArgs(propertyName);
                //PropertyChanged?.Invoke(this, e);
                RaiseDependentPropertyChangedEvents(propertyName);
            }
        }
        #endregion // INotifyPropertyChanged Members

        private IEnumerable<string> GetDependentPropertyList(string changedPropertyName)
        {
            var allProperties = GetType().GetProperties(BindingFlags.FlattenHierarchy |
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var changedProperties = new List<string>
            {
                changedPropertyName
            };

            bool found = false;
            while (!found)
            {
                // Assume we are going to not find another property
                found = false;
                foreach (var pi in allProperties)
                {
                    var dpas = pi.GetCustomAttributes(typeof(DependsOnAttribute)).Cast<DependsOnAttribute>();
                    foreach (var dpa in dpas)
                    {
                        if (changedProperties.Contains(dpa.Property))
                        {
                            changedProperties.Add(pi.Name);
                            found = true;
                        }
                    }
                }
            }

            return changedProperties.Skip(1);
        }

        private void RaiseDependentPropertyChangedEvents(string changedPropertyName)
        {
            var allProperties = GetType().GetProperties(BindingFlags.FlattenHierarchy |
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var changedProperties = new List<string>
            {
                changedPropertyName
            };

            bool found = true;
            while (!found)
            {
                // Assume we are going to not find another property
                found = false;
                foreach (var pi in allProperties)
                {
                    var dpas = pi.GetCustomAttributes(typeof(DependsOnAttribute)).Cast<DependsOnAttribute>();
                    foreach (var dpa in dpas)
                    {
                        if (changedProperties.Contains(dpa.Property))
                        {
                            changedProperties.Add(pi.Name);
                            found = true;
                        }
                    }
                }
            }

            foreach (var propertyName in changedProperties)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                PropertyChanged?.Invoke(this, e);
            }
        }

        bool IPropertyVerification.ThrowOnInvalidPropertyName => true;

        public bool IsInitializing { get; protected set; }
        private ISet<string> LostOnPropertyNotification { get; } = new HashSet<string>();

        #region ISupportInitialize

        private int _deferredInit = 0;

        public void BeginInit()
        {
            // The thread that increments to 1 will begin initialization process.
            if (Interlocked.Increment(ref _deferredInit) != 1) return;
            IsInitializing = true;
            IsInitialized = false;
        }

        public virtual void EndInit()
        {
            // The thread that decrements to 0 will end initialization process.
            if (Interlocked.Decrement(ref _deferredInit) != 0) return;

            IsInitializing = false;
            IsInitialized = true;

            lock (LostOnPropertyNotification)
            {
                foreach (var updatedProperty in LostOnPropertyNotification)
                {
                    //var e = new PropertyChangedEventArgs(updatedProperty);
                    //PropertyChanged?.Invoke(this, e);
                    RaiseDependentPropertyChangedEvents(updatedProperty);
                }
                LostOnPropertyNotification.Clear();
            }

            Initialized?.Invoke(this, EventArgs.Empty);
        }
        #endregion // ISupportInitialize

        #region ISupportInitializeNotification
        public bool IsInitialized { get; protected set; }
        public event EventHandler Initialized;
        #endregion // ISupportInitializeNotification

        protected const string IndexerName = "Item[]";
    }
}
