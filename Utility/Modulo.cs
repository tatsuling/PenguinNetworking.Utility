﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenguinNetworking.Utility
{
    public struct Modulo
    {
        public Modulo(int v, int mod)
        {
            if (v < 0)
            {
                _value = (v % mod) + mod;
            }
            else
            {
                _value = v;
            }
            Mod = mod;
        }

        public static Modulo operator +(Modulo a, int b)
        {
            return new Modulo(a._value + b, a.Mod);
        }

        public static Modulo operator -(Modulo a, int b)
        {
            return new Modulo(a._value + a.Mod - b, a.Mod);
        }

        public static Modulo operator *(Modulo a, int b)
        {
            if (b < 0)
            {
                b = (b % a.Mod) + a.Mod;
            }
            return new Modulo(a._value * b, a.Mod);
        }

        public static Modulo operator ++(Modulo a)
        {
            return new Modulo(a._value + 1, a.Mod);
        }

        public static Modulo operator --(Modulo a)
        {
            return new Modulo(a._value + a.Mod - 1, a.Mod);
        }

        public static implicit operator int(Modulo a)
        {
            return a.Value;
        }

        private readonly int _value;

        public int Value => _value >= Mod ? _value % Mod : _value;
        public int Mod { get; }

        public override string ToString()
        {
            return $"{Value} (mod {Mod})";
        }
    }
}
