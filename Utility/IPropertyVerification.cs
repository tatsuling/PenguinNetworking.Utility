﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace PenguinNetworking.Utility
{
    public interface IPropertyVerification
    {
        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might
        /// override this property's getter to return true.
        /// </summary>
        bool ThrowOnInvalidPropertyName { get; }
    }

    public static class PropertyVerificationExtension
    {
        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void VerifyPropertyName(this IPropertyVerification verification, string propertyName, bool publicOnly = false)
        {
            // Verify that the property name matches a real,
            // public, instance property on this object.
            var flags = BindingFlags.Public | BindingFlags.Instance;
            if (!publicOnly) flags |= BindingFlags.NonPublic;

            // Check for the special case of the indexer on collections.
            if (propertyName == IndexerName)
            {
                propertyName = "Item";
            }

            //Assert(verification.GetType().GetProperty(propertyName, flags) != null);
            var propertyInfo = verification.GetType()
                .GetProperties(flags)
                .FirstOrDefault(pi => pi.Name == propertyName);
            //var propertyInfo = verification.GetType().GetProperty(propertyName, flags);
            if (propertyInfo != null) return;

            var msg = $"Invalid property name '{propertyName}' on class {verification.GetType().Name}";

            if (verification.ThrowOnInvalidPropertyName)
                throw new Exception(msg);

            Debug.Fail(msg);
        }

        private const string IndexerName = "Item[]";
    }
}
